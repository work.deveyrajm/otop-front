import React, { useState } from "react";
import { AppBar, Typography, Toolbar, Grid, Box } from "@material-ui/core";
import { IconButton } from "@material-ui/core";
import MenuOpenIcon from "@material-ui/icons/MenuOpen";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import Button from "@material-ui/core/Button";
import logo from "../logo.png";
import {
  List,
  ListItem,
  //   ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import { Drawer } from "@material-ui/core";
import style from "../css-modules/navbar.module.css";
import { Link } from "react-router-dom";

const Navbar = () => {
  const [isToggled, setToggled] = useState(false);

  const DrawerMob = () => {
    const itemsList = [
      {
        text: "Sign In",
        PageRoute: "/SignIn",
      },
      {
        text: "Sign 3Up",
        PageRoute: "/SignUp",
      },
    ];

    return (
      <Drawer
        open={isToggled}
        onClose={() => setToggled(!isToggled)}
        anchor="right"
      >
        <List className={style.drawer_width}>
          {itemsList.map((item, index) => {
            const { text, PageRoute } = item;
            return (
              <Link to={PageRoute} style={{ textDecoration: "none" }}>
                <ListItem button key={text}>
                  <ListItemText primary={text} />
                </ListItem>
              </Link>
            );
          })}
        </List>
      </Drawer>
    );
  };

  return (
    <div>
      <DrawerMob />
      <AppBar>
        <Grid container direction="row" className={style.navbar}>
          <Grid item sm={2}></Grid>
          <Grid item xs={12} sm={8}>
            <Toolbar disableGutters>
              <Typography variant="h5" className={style.logo}>
                <Link to="/">
                  <img src={logo} alt="otopLogo" className={style.logo_image} />
                </Link>
              </Typography>

              <Link to="/Cart" style={{ textDecoration: "none" }}>
                <IconButton>
                  <ShoppingBasketIcon />
                </IconButton>
              </Link>

              <Box mr={2}>
                <Link to="/SignIn" style={{ textDecoration: "none" }}>
                  <Button color="primary" className={style.temp_hold}>
                    Sign In
                  </Button>
                </Link>
              </Box>
              <Box>
                <Link to="/SignUp" style={{ textDecoration: "none" }}>
                  <Button
                    variant="contained"
                    color="primary"
                    className={style.temp_hold}
                  >
                    Sign Up
                  </Button>
                </Link>
              </Box>
              <IconButton
                onClick={() => setToggled(!isToggled)}
                className={style.icon_style}
              >
                <MenuOpenIcon />
              </IconButton>
            </Toolbar>
          </Grid>
          <Grid item sm={2}></Grid>
        </Grid>
      </AppBar>
    </div>
  );
};

export default Navbar;
