import React from "react";
import {
  Drawer,
  List,
  ListItem,
  //   ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import style from "../css-modules/drawer.module.css";

const DrawerMob = () => {
  const itemsList = [
    {
      text: "Sign In",
    },
    {
      text: "Sign Up",
    },
  ];

  return (
    <Drawer open={false} anchor="right">
      <List className={style.drawer_width}>
        {itemsList.map((item, index) => {
          const { text } = item;
          return (
            <ListItem button key={text}>
              <ListItemText primary={text} />
            </ListItem>
          );
        })}
      </List>
    </Drawer>
  );
};

export default DrawerMob;
